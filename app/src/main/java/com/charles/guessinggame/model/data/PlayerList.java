package com.charles.guessinggame.model.data;

import java.util.ArrayList;
import java.util.List;

public class PlayerList
{

    private List<Player> players;
    private int playersDisplayed;

    public PlayerList()
    {
        players = new ArrayList<>();
        playersDisplayed = 2;
    }

    public List<Player> getPlayers()
    {
        return players;
    }

    public int getPlayersDisplayed()
    {
        return playersDisplayed;
    }

    public void setPlayers(List<Player> players)
    {
        this.players = players;
    }

    public void setPlayersDisplayed(int playersDisplayed)
    {
        this.playersDisplayed = playersDisplayed;
    }

    public List<Integer> findLargestPointsPerGameIndices()
    {
        double largestPointsPerGame = players.get(0).getPointsPerGame();
        List<Integer> largestIndices = new ArrayList<>();
        largestIndices.add(0);
        for(int i = 1; i < playersDisplayed; i++)
        {
            double pointsPerGame = players.get(i).getPointsPerGame();
            if(pointsPerGame > largestPointsPerGame)
            {
                largestPointsPerGame = pointsPerGame;
                largestIndices.clear();
                largestIndices.add(i);
            }
            else if(pointsPerGame == largestPointsPerGame)
            {
                largestIndices.add(i);
            }
        }
        return largestIndices;
    }

}