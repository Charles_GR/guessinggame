package com.charles.guessinggame.model;

import android.app.Application;
import com.charles.guessinggame.model.data.*;

public class GameApplication extends Application
{

    private static GameApplication instance;
    private PlayerList playerList;
    private Score score;
    private GameTimer gameTimer;

    public static GameApplication getInstance()
    {
        return instance;
    }

    public PlayerList getPlayerList()
    {
        return playerList;
    }

    public Score getScore()
    {
        return score;
    }

    public GameTimer getGameTimer()
    {
        return gameTimer;
    }

    public void resetGameTimer()
    {
        gameTimer = new GameTimer();
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
        playerList = new PlayerList();
        score = new Score();
        gameTimer = new GameTimer();
    }

}