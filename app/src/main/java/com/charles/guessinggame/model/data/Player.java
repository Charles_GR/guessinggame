package com.charles.guessinggame.model.data;

import com.google.gson.annotations.SerializedName;

public class Player
{

    @SerializedName("first_name") private String firstName;
    @SerializedName("last_name") private String lastName;
    @SerializedName("image_url") private String imageURL;
    @SerializedName("fppg") private double pointsPerGame;

    public Player(String firstName, String lastName, String imageURL, double pointsPerGame)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageURL = imageURL;
        this.pointsPerGame = pointsPerGame;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getImageURL()
    {
        return imageURL;
    }

    public double getPointsPerGame()
    {
        return pointsPerGame;
    }

    public String makeFullName()
    {
        return getFirstName() + " " + getLastName();
    }

    @Override
    public boolean equals(Object other)
    {
        if(other instanceof Player)
        {
            Player player = (Player)other;
            return firstName.equals(player.firstName) && lastName.equals(player.lastName) &&
                    imageURL.equals(player.imageURL) && pointsPerGame == player.pointsPerGame;
        }
        return false;
    }

}