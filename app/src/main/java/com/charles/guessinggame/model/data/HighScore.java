package com.charles.guessinggame.model.data;

import android.support.annotation.NonNull;
import java.util.Date;

public class HighScore implements Comparable<HighScore>
{

    private String player;
    private Score gameScore;
    private float gameTime;
    private Date dateCreated;

    public HighScore()
    {

    }

    public HighScore(String player, Score gameScore, float gameTime)
    {
        this.player = player;
        this.gameScore = gameScore;
        this.gameTime = gameTime;
        dateCreated = new Date();
    }

    public String getPlayer()
    {
        return player;
    }

    public Score getGameScore()
    {
        return gameScore;
    }

    public float getGameTime()
    {
        return gameTime;
    }

    public Date getDateCreated()
    {
        return dateCreated;
    }

    public void setPlayer(String player)
    {
        this.player = player;
    }

    @Override
    public int compareTo(@NonNull HighScore other)
    {
        int compare = gameScore.compareTo(other.getGameScore());
        compare = (compare == 0) ? Float.compare(other.getGameScore().getQuestionsAnswered() / other.getGameTime(), gameScore.getQuestionsAnswered() / gameTime) : compare;
        compare = (compare == 0) ? dateCreated.compareTo(other.getDateCreated()) : compare;
        return compare;
    }

    @Override
    public boolean equals(Object other)
    {
        if(other instanceof HighScore)
        {
            HighScore highScore = (HighScore)other;
            return gameScore.getQuestionsAnswered() == highScore.getGameScore().getQuestionsAnswered() &&
                    gameScore.getQuestionsCorrect() == highScore.getGameScore().getQuestionsCorrect() && gameTime == highScore.getGameTime();
        }
        return false;
    }

    @Override
    public String toString()
    {
        return player + ": " + getGameScore().getQuestionsCorrect() + " / " + getGameScore().getQuestionsAnswered() + " = " +
                getGameScore().writeFormattedPercentageCorrect() + " in " + Math.round(100f * gameTime) / 100f + "s";
    }

}