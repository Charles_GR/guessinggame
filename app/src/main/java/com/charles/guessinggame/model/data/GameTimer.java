package com.charles.guessinggame.model.data;

import android.os.SystemClock;

public class GameTimer
{

    private GameTimer pauseTimer;
    private boolean started;
    private boolean paused;
    private boolean finished;
    private float startTime;
    private float finishTime;
    private float pausedTime;

    public GameTimer()
    {
        pauseTimer = null;
        started = false;
        paused = false;
        finished = false;
        startTime = 0;
        finishTime = 0;
        pausedTime = 0;
    }

    public void start()
    {
        if(!started)
        {
            started = true;
            startTime = SystemClock.elapsedRealtime() / 1000f;
        }
    }

    public void stop()
    {
        if(started && !paused)
        {
            finished = true;
            finishTime = SystemClock.elapsedRealtime() / 1000f;
        }
    }

    public void pause()
    {
        if(started && !paused)
        {
            paused = true;
            pauseTimer = new GameTimer();
            pauseTimer.start();
        }
    }

    public void resume()
    {
        if(started && paused)
        {
            paused = false;
            pauseTimer.stop();
            pausedTime += pauseTimer.calcRunningTime();
        }
    }

    public float calcRunningTime()
    {
        if(!started || paused || !finished)
        {
            throw new IllegalStateException();
        }
        return finishTime - startTime - pausedTime;
    }

}