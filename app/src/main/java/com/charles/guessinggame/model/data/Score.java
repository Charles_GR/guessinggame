package com.charles.guessinggame.model.data;

import android.support.annotation.NonNull;
import java.util.Locale;

public class Score implements Comparable<Score>
{

    private int questionsAnswered;
    private int questionsCorrect;

    public Score()
    {
        questionsAnswered = 0;
        questionsCorrect = 0;
    }

    public int getQuestionsCorrect()
    {
        return questionsCorrect;
    }

    public int getQuestionsAnswered()
    {
        return questionsAnswered;
    }

    public void incrementQuestionsCorrect()
    {
        questionsCorrect++;
    }

    public void incrementQuestionsAnswered()
    {
        questionsAnswered++;
    }

    protected float calcPercentageCorrect()
    {
        return questionsAnswered == 0 ? 0 : 100f * questionsCorrect / questionsAnswered;
    }

    public String writeFormattedPercentageCorrect()
    {
        float percentage = Math.round(10f * calcPercentageCorrect()) / 10f;
        return (percentage == (long)percentage) ? String.format(Locale.UK, "%d", (long)percentage) + "%"
                                                : String.format("%s", percentage) + "%";
    }

    public void resetScore()
    {
        questionsCorrect = 0;
        questionsAnswered = 0;
    }

    @Override
    public int compareTo(@NonNull Score other)
    {
        int compare = Float.compare(other.calcPercentageCorrect(), calcPercentageCorrect());
        compare = (compare == 0 && questionsCorrect == 0) ? questionsAnswered - other.questionsAnswered : compare;
        compare = (compare == 0) ? other.questionsAnswered - questionsAnswered : compare;
        return compare;
    }

    @Override
    public boolean equals(Object other)
    {
        if(other instanceof Score)
        {
            Score score = (Score)other;
            return questionsAnswered == score.questionsAnswered && questionsCorrect == score.questionsCorrect;
        }
        return false;
    }

}