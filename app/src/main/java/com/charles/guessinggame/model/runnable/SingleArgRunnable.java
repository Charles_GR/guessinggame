package com.charles.guessinggame.model.runnable;

public interface SingleArgRunnable<T>
{

    void run(T arg);

}