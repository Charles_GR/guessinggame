package com.charles.guessinggame.processing.util;

import com.charles.guessinggame.model.data.Player;
import com.google.gson.Gson;
import org.json.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public final class PlayerFetcher
{

    private static final String JSON_URL = "https://cdn.rawgit.com/liamjdouglas/bb40ee8721f1a9313c22c6ea0851a105/raw/6b6fc89d55ebe4d9b05c1469349af33651d7e7f1/Player.json";

    private PlayerFetcher()
    {
        throw new UnsupportedOperationException();
    }

    public static List<Player> fetchPlayers() throws IOException, JSONException
    {
        HttpURLConnection urlConnection = (HttpURLConnection)new URL(JSON_URL).openConnection();
        try
        {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String charset = parseCharset(urlConnection.getRequestProperty("Content-Type"));
            String jsonText = new String(StreamUtils.readUnknownFully(in), charset);
            JSONArray playersArray = new JSONObject(jsonText).getJSONArray("players");
            List<Player> playersList = new ArrayList<>();
            for(int i = 0; i < playersArray.length(); i++)
            {
                String imageURL = playersArray.getJSONObject(i).getJSONObject("images").getJSONObject("default").getString("url");
                playersList.add(new Gson().fromJson(playersArray.getJSONObject(i).put("image_url", imageURL).toString(), Player.class));
            }
            return playersList;
        }
        finally
        {
            urlConnection.disconnect();
        }
    }

    private static String parseCharset(String contentType)
    {
        if(contentType != null)
        {
            String[] params = contentType.split(",");
            for(int i = 1; i < params.length; i++)
            {
                String[] pair = params[i].trim().split("=");
                if(pair.length == 2 && pair[0].equals("charset"))
                {
                    return pair[1];
                }
            }
        }
        return "UTF-8";
    }

}