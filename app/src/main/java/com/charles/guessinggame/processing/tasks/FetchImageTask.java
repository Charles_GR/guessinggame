package com.charles.guessinggame.processing.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import com.charles.guessinggame.R;
import com.charles.guessinggame.model.GameApplication;
import com.charles.guessinggame.processing.util.ImageFetcher;
import java.io.IOException;

public class FetchImageTask extends AsyncTask<Void, Void, Bitmap>
{

    private static final Bitmap loading = BitmapFactory.decodeResource(GameApplication.getInstance().getResources(), R.mipmap.loading);
    private static final Bitmap notAvailable = BitmapFactory.decodeResource(GameApplication.getInstance().getResources(), R.mipmap.no_image_available);
    private String imageURL;
    private ImageView imageView;

    public FetchImageTask(String imageURL, ImageView imageView)
    {
        this.imageURL = imageURL;
        this.imageView = imageView;
    }

    @Override
    protected void onPreExecute()
    {
        imageView.setImageBitmap(loading);
    }

    @Override
    protected Bitmap doInBackground(Void... voids)
    {
        try
        {
            Bitmap bitmap = ImageFetcher.fetchImage(imageURL);
            return bitmap == null ? notAvailable : bitmap;
        }
        catch(IOException e)
        {
            return notAvailable;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap)
    {
        imageView.setImageBitmap(bitmap);
    }

}