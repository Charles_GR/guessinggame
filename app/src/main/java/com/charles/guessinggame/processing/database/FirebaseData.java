package com.charles.guessinggame.processing.database;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public final class FirebaseData
{

    public static final DatabaseReference baseRef = FirebaseDatabase.getInstance().getReference();
    public static final DatabaseReference highScoresRef = baseRef.child("highScores");

    private FirebaseData()
    {
        throw new UnsupportedOperationException();
    }

}