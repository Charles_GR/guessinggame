package com.charles.guessinggame.processing.tasks;

import android.os.AsyncTask;
import org.json.JSONException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.charles.guessinggame.model.data.Player;
import com.charles.guessinggame.model.runnable.ListRunnable;
import com.charles.guessinggame.processing.util.PlayerFetcher;

public class FetchPlayersTask extends AsyncTask<Void, Void, List<Player>>
{

    private boolean success;
    private ListRunnable<Player> onSuccess;
    private Runnable onError;

    public FetchPlayersTask(ListRunnable<Player> onSuccess, Runnable onError)
    {
        success = true;
        this.onSuccess = onSuccess;
        this.onError = onError;
    }

    @Override
    protected List<Player> doInBackground(Void... voids)
    {
        List<Player> players = new ArrayList<>();
        try
        {
            players = PlayerFetcher.fetchPlayers();
        }
        catch(IOException | JSONException e)
        {
            success = false;
        }
        return players;
    }

    @Override
    protected void onPostExecute(List<Player> players)
    {
        if(success)
        {
            onSuccess.run(players);
        }
        else
        {
            onError.run();
        }
    }

}