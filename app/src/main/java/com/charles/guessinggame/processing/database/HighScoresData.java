package com.charles.guessinggame.processing.database;

import com.charles.guessinggame.model.data.HighScore;
import com.charles.guessinggame.model.runnable.ListRunnable;
import com.charles.guessinggame.model.runnable.SingleArgRunnable;
import com.google.firebase.database.*;
import java.util.ArrayList;
import java.util.List;

public final class HighScoresData
{

    private HighScoresData()
    {
        throw new UnsupportedOperationException();
    }

    public static void fetchHighScores(final ListRunnable<HighScore> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.highScoresRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<HighScore> highScores = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    highScores.add(child.getValue(HighScore.class));
                }
                onSuccess.run(highScores);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                onError.run(databaseError.getMessage());
            }
        });
    }

    public static void postHighScore(HighScore highScore)
    {
        FirebaseData.highScoresRef.push().setValue(highScore);
    }

}