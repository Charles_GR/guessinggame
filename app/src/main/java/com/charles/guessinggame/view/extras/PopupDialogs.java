package com.charles.guessinggame.view.extras;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.charles.guessinggame.R;
import com.charles.guessinggame.model.runnable.SingleArgRunnable;

public final class PopupDialogs
{

    private PopupDialogs()
    {
        throw new UnsupportedOperationException();
    }

    public static void showInputDialog(Context context, String title, String description, String inputLabel, String positiveButtonText, String negativeButtonText,
                                       final SingleArgRunnable<String> onPositiveButtonPressed, final Runnable onNegativeButtonPressed)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        View inputView = View.inflate(context, R.layout.layout_input_popup, null);
        alertDialogBuilder.setView(inputView);
        TextView tvDescription = inputView.findViewById(R.id.tvDescription);
        TextView tvInputLabel = inputView.findViewById(R.id.tvInputLabel);
        tvDescription.setText(description);
        final EditText etInputValue = inputView.findViewById(R.id.etInputValue);
        tvInputLabel.setText(inputLabel);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(positiveButtonText, new OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                onPositiveButtonPressed.run(etInputValue.getText().toString());
            }
        });
        alertDialogBuilder.setNegativeButton(negativeButtonText, new OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                dialog.cancel();
                onNegativeButtonPressed.run();
            }
        });
        alertDialogBuilder.create().show();
    }

}