package com.charles.guessinggame.view.views;

import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import com.charles.guessinggame.R;
import com.charles.guessinggame.model.data.HighScore;
import com.charles.guessinggame.model.runnable.ListRunnable;
import com.charles.guessinggame.model.runnable.SingleArgRunnable;
import com.charles.guessinggame.processing.database.HighScoresData;
import com.charles.guessinggame.view.extras.PopupDialogs;
import com.google.gson.Gson;
import java.util.*;

public class HighScoresActivity extends Activity
{

    public static final String PENDING_HIGH_SCORE_TAG = "PendingHighScore";
    private ListView lvHighScores;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_scores);
        lvHighScores = findViewById(R.id.lvHighScores);
        if(getActionBar() != null)
        {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        HighScoresData.fetchHighScores(fetchHighScoresOnSuccess, fetchHighScoresOnError);
    }

    private boolean pendingHighScoreIsAdded(List<HighScore> highScores)
    {
        if(!getIntent().hasExtra(PENDING_HIGH_SCORE_TAG))
        {
            return false;
        }
        String pendingHighScoreJson = getIntent().getStringExtra(PENDING_HIGH_SCORE_TAG);
        getIntent().removeExtra(PENDING_HIGH_SCORE_TAG);
        HighScore newHighScore = new Gson().fromJson(pendingHighScoreJson, HighScore.class);
        if(highScores.size() < 10 || newHighScore.compareTo(highScores.get(9)) < 0)
        {
            getNameForHighScore(newHighScore);
            return true;
        }
        else
        {
            Toast.makeText(HighScoresActivity.this, R.string.not_made_high_scores, Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void getNameForHighScore(final HighScore highScore)
    {
        PopupDialogs.showInputDialog(this, getString(R.string.high_score), getString(R.string.well_done_high_score),
                getString(R.string.name), getString(R.string.save), getString(R.string.cancel),
                new SingleArgRunnable<String>()
                {
                    @Override
                    public void run(String text)
                    {
                        highScore.setPlayer(text);
                        HighScoresData.postHighScore(highScore);
                    }
                },
                new Runnable()
                {
                    public void run()
                    {
                        startActivity(getIntent());
                    }
                });
    }

    private final ListRunnable<HighScore> fetchHighScoresOnSuccess = new ListRunnable<HighScore>()
    {
        @Override
        public void run(List<HighScore> highScores)
        {
            List<String> highScoreStrings = new ArrayList<>();
            Collections.sort(highScores);
            if(pendingHighScoreIsAdded(highScores))
            {
                return;
            }
            if(highScores.isEmpty())
            {
                Toast.makeText(HighScoresActivity.this, R.string.no_high_scores_yet, Toast.LENGTH_SHORT).show();
                finish();
                return;
            }
            for(int i = 0; i < 10 && i < highScores.size(); i++)
            {
                highScoreStrings.add(i + 1 + ". " + highScores.get(i).toString());
            }
            lvHighScores.setAdapter(new ArrayAdapter<>(HighScoresActivity.this, android.R.layout.simple_list_item_1, highScoreStrings));
        }
    };

    private final SingleArgRunnable<String> fetchHighScoresOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            Toast.makeText(HighScoresActivity.this, text, Toast.LENGTH_SHORT).show();
            finish();
        }
    };

}