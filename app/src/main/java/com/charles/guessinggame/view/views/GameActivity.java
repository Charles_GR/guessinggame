package com.charles.guessinggame.view.views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.charles.guessinggame.R;
import com.charles.guessinggame.model.GameApplication;
import com.charles.guessinggame.model.data.HighScore;
import com.charles.guessinggame.model.data.Player;
import com.charles.guessinggame.model.runnable.ListRunnable;
import com.charles.guessinggame.processing.tasks.FetchImageTask;
import com.charles.guessinggame.processing.tasks.FetchPlayersTask;
import com.google.gson.Gson;
import java.util.*;

public class GameActivity extends Activity implements OnClickListener, OnLongClickListener
{

    private ImageButton btnPrevious;
    private ImageButton btnNext;
    private ImageButton btnSelect;
    private TextView tvTotalScore;
    private ViewFlipper vfPlayers;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        btnPrevious = findViewById(R.id.btnPrevious);
        btnNext = findViewById(R.id.btnNext);
        btnSelect = findViewById(R.id.btnSelect);
        tvTotalScore = findViewById(R.id.tvTotalScore);
        vfPlayers = findViewById(R.id.vfPlayers);
        btnPrevious.setOnClickListener(this);
        btnPrevious.setOnLongClickListener(this);
        btnNext.setOnClickListener(this);
        btnNext.setOnLongClickListener(this);
        btnSelect.setOnClickListener(this);
        btnSelect.setOnLongClickListener(this);
        tvTotalScore.setText(getResources().getString(R.string.total_score_value, 0, 0));
        vfPlayers.setInAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
        vfPlayers.setOutAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));
        if(getActionBar() != null && getActionBar().getCustomView() == null)
        {
            View customView = View.inflate(this, R.layout.action_layout_main, null);
            TextView tvTitle = customView.findViewById(R.id.tvTitle);
            ImageButton btnHighScores = customView.findViewById(R.id.btnHighScores);
            tvTitle.setText(R.string.app_name);
            btnHighScores.setOnClickListener(this);
            btnHighScores.setOnLongClickListener(this);
            getActionBar().setDisplayShowCustomEnabled(true);
            getActionBar().setCustomView(customView);
        }
        GameApplication.getInstance().getScore().resetScore();
        setAllButtonsEnabled(false);
        new FetchPlayersTask(fetchPlayersOnSuccess, fetchPlayersOnError).execute();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        GameApplication.getInstance().getGameTimer().pause();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        GameApplication.getInstance().getGameTimer().resume();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        GameApplication.getInstance().resetGameTimer();
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btnHighScores:
                startActivity(new Intent(this, HighScoresActivity.class));
                break;
            case R.id.btnPrevious:
                vfPlayers.setDisplayedChild(vfPlayers.getDisplayedChild() - 1);
                setPreviousAndNextButtonsEnabled();
                break;
            case R.id.btnNext:
                vfPlayers.setDisplayedChild(vfPlayers.getDisplayedChild() + 1);
                setPreviousAndNextButtonsEnabled();
                break;
            case R.id.btnSelect:
                setAllButtonsEnabled(false);
                GameApplication.getInstance().getScore().incrementQuestionsAnswered();
                checkAnswer(vfPlayers.getDisplayedChild());
                break;
        }
    }

    @Override
    public boolean onLongClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btnHighScores:
                Toast.makeText(this, R.string.high_scores, Toast.LENGTH_SHORT).show();
                return true;
            case R.id.btnPrevious:
                Toast.makeText(this, R.string.previous, Toast.LENGTH_SHORT).show();
                return true;
            case R.id.btnNext:
                Toast.makeText(this, R.string.next, Toast.LENGTH_SHORT).show();
                return true;
            case R.id.btnSelect:
                Toast.makeText(this, R.string.select_player, Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }
    }

    private void createPlayerList()
    {
        vfPlayers.removeAllViews();
        Collections.shuffle(GameApplication.getInstance().getPlayerList().getPlayers());
        GameApplication.getInstance().getPlayerList().setPlayersDisplayed(2 + Math.round(GameApplication.getInstance().getScore().getQuestionsCorrect() / 4));
        for(int i = 0; i <  GameApplication.getInstance().getPlayerList().getPlayersDisplayed(); i++)
        {
            displayPlayerInfo(GameApplication.getInstance().getPlayerList().getPlayers().get(i));
        }
        setPreviousAndNextButtonsEnabled();
        btnSelect.setEnabled(true);
    }

    private void displayPlayerInfo(Player player)
    {
        View view = View.inflate(this, R.layout.layout_player, null);
        TextView tvFullName = view.findViewById(R.id.tvFullNameValue);
        ImageView ivImage = view.findViewById(R.id.ivImage);
        TextView tvPointsPerGame = view.findViewById(R.id.tvPointsPerGameValue);
        tvFullName.setText(player.makeFullName());
        new FetchImageTask(player.getImageURL(), ivImage).execute();
        tvPointsPerGame.setText(String.valueOf(Math.round(100d * player.getPointsPerGame()) / 100d));
        vfPlayers.addView(view);
    }

    private void checkAnswer(int selection)
    {
        if(GameApplication.getInstance().getPlayerList().findLargestPointsPerGameIndices().contains(selection))
        {
            Toast.makeText(this, R.string.correct, Toast.LENGTH_SHORT).show();
            GameApplication.getInstance().getScore().incrementQuestionsCorrect();
            if(GameApplication.getInstance().getScore().getQuestionsCorrect() == 10)
            {
                endQuiz();
                return;
            }
        }
        else
        {
            Toast.makeText(this, R.string.incorrect, Toast.LENGTH_SHORT).show();
        }
        tvTotalScore.setText(getResources().getString(R.string.total_score_value,
                GameApplication.getInstance().getScore().getQuestionsCorrect(), GameApplication.getInstance().getScore().getQuestionsAnswered()));
        advanceQuiz(selection);
    }

    private void advanceQuiz(final int selection)
    {
        vfPlayers.getChildAt(selection).findViewById(R.id.llPointsPerGame).setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable()
        {
            public void run()
            {
                vfPlayers.getChildAt(selection).findViewById(R.id.llPointsPerGame).setVisibility(View.GONE);
                createPlayerList();
            }
        }, 3000);
    }

    private void endQuiz()
    {
        GameApplication.getInstance().getGameTimer().stop();
        setAllButtonsEnabled(false);
        displayAndSaveScore();
    }

    private void displayAndSaveScore()
    {
        float time = GameApplication.getInstance().getGameTimer().calcRunningTime();
        GameApplication.getInstance().resetGameTimer();
        Toast.makeText(this, getResources().getString(R.string.your_score_value, GameApplication.getInstance().getScore().getQuestionsCorrect(),
                GameApplication.getInstance().getScore().getQuestionsAnswered(), time), Toast.LENGTH_SHORT).show();
        HighScore pendingHighScore = new HighScore(null, GameApplication.getInstance().getScore(), time);
        finish();
        startActivity(getIntent());
        Intent intent = new Intent(GameActivity.this, HighScoresActivity.class);
        intent.putExtra(HighScoresActivity.PENDING_HIGH_SCORE_TAG, new Gson().toJson(pendingHighScore));
        startActivity(intent);
    }

    private void setPreviousAndNextButtonsEnabled()
    {
        btnPrevious.setEnabled(vfPlayers.getDisplayedChild() > 0);
        btnNext.setEnabled(vfPlayers.getDisplayedChild() < vfPlayers.getChildCount() - 1);
    }

    private void setAllButtonsEnabled(boolean enabled)
    {
        btnPrevious.setEnabled(enabled);
        btnNext.setEnabled(enabled);
        btnSelect.setEnabled(enabled);
    }

    private final ListRunnable<Player> fetchPlayersOnSuccess = new ListRunnable<Player>()
    {
        @Override
        public void run(List<Player> players)
        {
            GameApplication.getInstance().getPlayerList().setPlayers(players);
            createPlayerList();
            setAllButtonsEnabled(true);
            GameApplication.getInstance().getGameTimer().start();
        }
    };

    private final Runnable fetchPlayersOnError = new Runnable()
    {
        @Override
        public void run()
        {
            Toast.makeText(GameActivity.this, R.string.fetch_error, Toast.LENGTH_SHORT).show();
        }
    };

}