package com.charles.guessinggame;

import com.charles.guessinggame.model.data.Player;
import com.charles.guessinggame.processing.util.PlayerFetcher;
import org.junit.Test;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PlayerFetcherUnitTest
{

    @Test
    public void fetchPlayers_isCorrect() throws Exception
    {
        List<Player> players = PlayerFetcher.fetchPlayers();
        assertEquals(players.size(), 59);
        assertEquals(new Player("Stephen", "Curry", "https://d17odppiik753x.cloudfront.net/playerimages/nba/9524.png", 47.94303797468354), players.get(0));
        assertEquals(new Player("Draymond", "Green", "https://d17odppiik753x.cloudfront.net/playerimages/nba/15860.png", 38.9604938271605), players.get(1));
        assertEquals(new Player("Damian", "Lillard", "https://d17odppiik753x.cloudfront.net/playerimages/nba/20848.png", 39.37866666666667), players.get(2));
        assertEquals(new Player("Hassan", "Whiteside", "https://d17odppiik753x.cloudfront.net/playerimages/nba/12363.png", 35.75342465753425), players.get(3));
        assertEquals(new Player("Klay", "Thompson", "https://d17odppiik753x.cloudfront.net/playerimages/nba/14509.png", 30.839999999999996), players.get(4));
    }

}