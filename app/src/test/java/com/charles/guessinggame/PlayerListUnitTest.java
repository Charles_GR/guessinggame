package com.charles.guessinggame;

import com.charles.guessinggame.model.data.Player;
import com.charles.guessinggame.model.data.PlayerList;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;

import static org.junit.Assert.*;

public class PlayerListUnitTest
{

    private static PlayerList playerList;

    @Before
    public void setup()
    {
        playerList = new PlayerList();
        playerList.getPlayers().add(new Player("NotNeeded", "NotNeeded", "NotNeeded", 37.84));
        playerList.getPlayers().add(new Player("NotNeeded", "NotNeeded", "NotNeeded", 5.8));
        playerList.getPlayers().add(new Player("NotNeeded", "NotNeeded", "NotNeeded", 37.84));
        playerList.getPlayers().add(new Player("NotNeeded", "NotNeeded", "NotNeeded", 63.58));
        playerList.getPlayers().add(new Player("NotNeeded", "NotNeeded", "NotNeeded", 63.57));
    }

    @Test
    public void findLargestPointsPerGameIndices_isCorrect() throws Exception
    {
        playerList.setPlayersDisplayed(2);
        assertEquals(Arrays.asList(new Integer[]{0}), playerList.findLargestPointsPerGameIndices());
        playerList.setPlayersDisplayed(3);
        assertEquals(Arrays.asList(new Integer[]{0, 2}), playerList.findLargestPointsPerGameIndices());
        playerList.setPlayersDisplayed(4);
        assertEquals(Arrays.asList(new Integer[]{3}), playerList.findLargestPointsPerGameIndices());
        playerList.setPlayersDisplayed(5);
        assertEquals(Arrays.asList(new Integer[]{3}), playerList.findLargestPointsPerGameIndices());
    }

}


